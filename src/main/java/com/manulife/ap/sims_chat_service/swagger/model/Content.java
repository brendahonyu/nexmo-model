package com.manulife.ap.sims_chat_service.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.manulife.ap.sims_chat_service.swagger.model.Location;
import com.manulife.ap.sims_chat_service.swagger.model.MultiMedia;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Defines content information.
 */
@ApiModel(description = "Defines content information.")
@Validated

public class Content  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("type")
  private String type = null;

  @JsonProperty("text")
  private String text = null;

  @JsonProperty("image")
  private MultiMedia image = null;

  @JsonProperty("audio")
  private MultiMedia audio = null;

  @JsonProperty("video")
  private MultiMedia video = null;

  @JsonProperty("file")
  private MultiMedia file = null;

  @JsonProperty("location")
  private Location location = null;

  public Content type(String type) {
    this.type = type;
    return this;
  }

  /**
   * Get type
   * @return type
  **/
  @ApiModelProperty(value = "")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Content text(String text) {
    this.text = text;
    return this;
  }

  /**
   * Get text
   * @return text
  **/
  @ApiModelProperty(value = "")


  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public Content image(MultiMedia image) {
    this.image = image;
    return this;
  }

  /**
   * Get image
   * @return image
  **/
  @ApiModelProperty(value = "")

  @Valid

  public MultiMedia getImage() {
    return image;
  }

  public void setImage(MultiMedia image) {
    this.image = image;
  }

  public Content audio(MultiMedia audio) {
    this.audio = audio;
    return this;
  }

  /**
   * Get audio
   * @return audio
  **/
  @ApiModelProperty(value = "")

  @Valid

  public MultiMedia getAudio() {
    return audio;
  }

  public void setAudio(MultiMedia audio) {
    this.audio = audio;
  }

  public Content video(MultiMedia video) {
    this.video = video;
    return this;
  }

  /**
   * Get video
   * @return video
  **/
  @ApiModelProperty(value = "")

  @Valid

  public MultiMedia getVideo() {
    return video;
  }

  public void setVideo(MultiMedia video) {
    this.video = video;
  }

  public Content file(MultiMedia file) {
    this.file = file;
    return this;
  }

  /**
   * Get file
   * @return file
  **/
  @ApiModelProperty(value = "")

  @Valid

  public MultiMedia getFile() {
    return file;
  }

  public void setFile(MultiMedia file) {
    this.file = file;
  }

  public Content location(Location location) {
    this.location = location;
    return this;
  }

  /**
   * Get location
   * @return location
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Content content = (Content) o;
    return Objects.equals(this.type, content.type) &&
        Objects.equals(this.text, content.text) &&
        Objects.equals(this.image, content.image) &&
        Objects.equals(this.audio, content.audio) &&
        Objects.equals(this.video, content.video) &&
        Objects.equals(this.file, content.file) &&
        Objects.equals(this.location, content.location);
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, text, image, audio, video, file, location);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Content {\n");
    
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    text: ").append(toIndentedString(text)).append("\n");
    sb.append("    image: ").append(toIndentedString(image)).append("\n");
    sb.append("    audio: ").append(toIndentedString(audio)).append("\n");
    sb.append("    video: ").append(toIndentedString(video)).append("\n");
    sb.append("    file: ").append(toIndentedString(file)).append("\n");
    sb.append("    location: ").append(toIndentedString(location)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

