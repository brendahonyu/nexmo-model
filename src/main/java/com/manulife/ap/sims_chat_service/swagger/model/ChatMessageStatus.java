package com.manulife.ap.sims_chat_service.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.manulife.ap.sims_chat_service.swagger.model.Error;
import com.manulife.ap.sims_chat_service.swagger.model.Usage;
import com.manulife.ap.sims_chat_service.swagger.model.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.joda.time.DateTime;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Defines chat message status information.
 */
@ApiModel(description = "Defines chat message status information.")
@Validated

public class ChatMessageStatus  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("message_uuid")
  private String messageUuid = null;

  @JsonProperty("from")
  private User from = null;

  @JsonProperty("to")
  private User to = null;

  @JsonProperty("timestamp")
  private DateTime timestamp = null;

  @JsonProperty("status")
  private String status = null;

  @JsonProperty("error")
  private Error error = null;

  @JsonProperty("usage")
  private Usage usage = null;

  @JsonProperty("client_ref")
  private String clientRef = null;

  @JsonProperty("modelClass")
  private String modelClass = null;

  public ChatMessageStatus messageUuid(String messageUuid) {
    this.messageUuid = messageUuid;
    return this;
  }

  /**
   * Get messageUuid
   * @return messageUuid
  **/
  @ApiModelProperty(value = "")


  public String getMessageUuid() {
    return messageUuid;
  }

  public void setMessageUuid(String messageUuid) {
    this.messageUuid = messageUuid;
  }

  public ChatMessageStatus from(User from) {
    this.from = from;
    return this;
  }

  /**
   * Get from
   * @return from
  **/
  @ApiModelProperty(value = "")

  @Valid

  public User getFrom() {
    return from;
  }

  public void setFrom(User from) {
    this.from = from;
  }

  public ChatMessageStatus to(User to) {
    this.to = to;
    return this;
  }

  /**
   * Get to
   * @return to
  **/
  @ApiModelProperty(value = "")

  @Valid

  public User getTo() {
    return to;
  }

  public void setTo(User to) {
    this.to = to;
  }

  public ChatMessageStatus timestamp(DateTime timestamp) {
    this.timestamp = timestamp;
    return this;
  }

  /**
   * Get timestamp
   * @return timestamp
  **/
  @ApiModelProperty(value = "")

  @Valid

  public DateTime getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(DateTime timestamp) {
    this.timestamp = timestamp;
  }

  public ChatMessageStatus status(String status) {
    this.status = status;
    return this;
  }

  /**
   * Get status
   * @return status
  **/
  @ApiModelProperty(value = "")


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public ChatMessageStatus error(Error error) {
    this.error = error;
    return this;
  }

  /**
   * Get error
   * @return error
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Error getError() {
    return error;
  }

  public void setError(Error error) {
    this.error = error;
  }

  public ChatMessageStatus usage(Usage usage) {
    this.usage = usage;
    return this;
  }

  /**
   * Get usage
   * @return usage
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Usage getUsage() {
    return usage;
  }

  public void setUsage(Usage usage) {
    this.usage = usage;
  }

  public ChatMessageStatus clientRef(String clientRef) {
    this.clientRef = clientRef;
    return this;
  }

  /**
   * Get clientRef
   * @return clientRef
  **/
  @ApiModelProperty(value = "")


  public String getClientRef() {
    return clientRef;
  }

  public void setClientRef(String clientRef) {
    this.clientRef = clientRef;
  }

  public ChatMessageStatus modelClass(String modelClass) {
    this.modelClass = modelClass;
    return this;
  }

  /**
   * Get modelClass
   * @return modelClass
  **/
  @ApiModelProperty(value = "")


  public String getModelClass() {
    return modelClass;
  }

  public void setModelClass(String modelClass) {
    this.modelClass = modelClass;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ChatMessageStatus chatMessageStatus = (ChatMessageStatus) o;
    return Objects.equals(this.messageUuid, chatMessageStatus.messageUuid) &&
        Objects.equals(this.from, chatMessageStatus.from) &&
        Objects.equals(this.to, chatMessageStatus.to) &&
        Objects.equals(this.timestamp, chatMessageStatus.timestamp) &&
        Objects.equals(this.status, chatMessageStatus.status) &&
        Objects.equals(this.error, chatMessageStatus.error) &&
        Objects.equals(this.usage, chatMessageStatus.usage) &&
        Objects.equals(this.clientRef, chatMessageStatus.clientRef) &&
        Objects.equals(this.modelClass, chatMessageStatus.modelClass);
  }

  @Override
  public int hashCode() {
    return Objects.hash(messageUuid, from, to, timestamp, status, error, usage, clientRef, modelClass);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ChatMessageStatus {\n");
    
    sb.append("    messageUuid: ").append(toIndentedString(messageUuid)).append("\n");
    sb.append("    from: ").append(toIndentedString(from)).append("\n");
    sb.append("    to: ").append(toIndentedString(to)).append("\n");
    sb.append("    timestamp: ").append(toIndentedString(timestamp)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    error: ").append(toIndentedString(error)).append("\n");
    sb.append("    usage: ").append(toIndentedString(usage)).append("\n");
    sb.append("    clientRef: ").append(toIndentedString(clientRef)).append("\n");
    sb.append("    modelClass: ").append(toIndentedString(modelClass)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

