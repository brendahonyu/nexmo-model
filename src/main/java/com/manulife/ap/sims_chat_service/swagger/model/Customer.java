package com.manulife.ap.sims_chat_service.swagger.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.querydsl.core.annotations.QueryEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Defines a chat customer information.
 */
@ApiModel(description = "Defines a chat customer information.")
@Validated
@QueryEntity
@Document(collection = "customers")
public class Customer  implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("chatappUsers")
  @Valid
  private List<User> chatappUsers = null;

  @JsonProperty("createdTimestamp")
  private DateTime createdTimestamp = null;

  @JsonProperty("updatedTimestamp")
  private DateTime updatedTimestamp = null;

  @JsonProperty("employeeId")
  private String employeeId = null;

  @JsonProperty("isTagged")
  private Boolean isTagged = null;

  @JsonProperty("isClosed")
  private Boolean isClosed = null;

  @JsonProperty("otp")
  private String otp = null;

  @JsonProperty("otpTimestamp")
  private DateTime otpTimestamp = null;

  @JsonProperty("otpSentTo")
  private String otpSentTo = null;

  @JsonProperty("otpVerifiedTimestamp")
  private DateTime otpVerifiedTimestamp = null;

  @JsonProperty("chatMessages")
  @Valid
  private List<ChatMessage> chatMessages = null;

  @JsonProperty("chatMessageUnreadCount")
  private BigDecimal chatMessageUnreadCount = null;

  @JsonProperty("chatMessageLastReceivedTimestamp")
  private DateTime chatMessageLastReceivedTimestamp = null;

  @JsonProperty("chatMessageOldestUnreadTimestamp")
  private DateTime chatMessageOldestUnreadTimestamp = null;

  @JsonProperty("modelClass")
  private String modelClass = null;

  public Customer id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Customer chatappUsers(List<User> chatappUsers) {
    this.chatappUsers = chatappUsers;
    return this;
  }

  public Customer addChatappUsersItem(User chatappUsersItem) {
    if (this.chatappUsers == null) {
      this.chatappUsers = new ArrayList<User>();
    }
    this.chatappUsers.add(chatappUsersItem);
    return this;
  }

  /**
   * Get chatappUsers
   * @return chatappUsers
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<User> getChatappUsers() {
    return chatappUsers;
  }

  public void setChatappUsers(List<User> chatappUsers) {
    this.chatappUsers = chatappUsers;
  }

  public Customer createdTimestamp(DateTime createdTimestamp) {
    this.createdTimestamp = createdTimestamp;
    return this;
  }

  /**
   * Get createdTimestamp
   * @return createdTimestamp
  **/
  @ApiModelProperty(value = "")

  @Valid

  public DateTime getCreatedTimestamp() {
    return createdTimestamp;
  }

  public void setCreatedTimestamp(DateTime createdTimestamp) {
    this.createdTimestamp = createdTimestamp;
  }

  public Customer updatedTimestamp(DateTime updatedTimestamp) {
    this.updatedTimestamp = updatedTimestamp;
    return this;
  }

  /**
   * Get updatedTimestamp
   * @return updatedTimestamp
  **/
  @ApiModelProperty(value = "")

  @Valid

  public DateTime getUpdatedTimestamp() {
    return updatedTimestamp;
  }

  public void setUpdatedTimestamp(DateTime updatedTimestamp) {
    this.updatedTimestamp = updatedTimestamp;
  }

  public Customer employeeId(String employeeId) {
    this.employeeId = employeeId;
    return this;
  }

  /**
   * Get employeeId
   * @return employeeId
  **/
  @ApiModelProperty(value = "")


  public String getEmployeeId() {
    return employeeId;
  }

  public void setEmployeeId(String employeeId) {
    this.employeeId = employeeId;
  }

  public Customer isTagged(Boolean isTagged) {
    this.isTagged = isTagged;
    return this;
  }

  /**
   * Get isTagged
   * @return isTagged
  **/
  @ApiModelProperty(value = "")


  public Boolean isIsTagged() {
    return isTagged;
  }

  public void setIsTagged(Boolean isTagged) {
    this.isTagged = isTagged;
  }

  public Customer isClosed(Boolean isClosed) {
    this.isClosed = isClosed;
    return this;
  }

  /**
   * Get isClosed
   * @return isClosed
  **/
  @ApiModelProperty(value = "")


  public Boolean isIsClosed() {
    return isClosed;
  }

  public void setIsClosed(Boolean isClosed) {
    this.isClosed = isClosed;
  }

  public Customer otp(String otp) {
    this.otp = otp;
    return this;
  }

  /**
   * Get otp
   * @return otp
  **/
  @ApiModelProperty(value = "")


  public String getOtp() {
    return otp;
  }

  public void setOtp(String otp) {
    this.otp = otp;
  }

  public Customer otpTimestamp(DateTime otpTimestamp) {
    this.otpTimestamp = otpTimestamp;
    return this;
  }

  /**
   * Get otpTimestamp
   * @return otpTimestamp
  **/
  @ApiModelProperty(value = "")

  @Valid

  public DateTime getOtpTimestamp() {
    return otpTimestamp;
  }

  public void setOtpTimestamp(DateTime otpTimestamp) {
    this.otpTimestamp = otpTimestamp;
  }

  public Customer otpSentTo(String otpSentTo) {
    this.otpSentTo = otpSentTo;
    return this;
  }

  /**
   * Get otpSentTo
   * @return otpSentTo
  **/
  @ApiModelProperty(value = "")


  public String getOtpSentTo() {
    return otpSentTo;
  }

  public void setOtpSentTo(String otpSentTo) {
    this.otpSentTo = otpSentTo;
  }

  public Customer otpVerifiedTimestamp(DateTime otpVerifiedTimestamp) {
    this.otpVerifiedTimestamp = otpVerifiedTimestamp;
    return this;
  }

  /**
   * Get otpVerifiedTimestamp
   * @return otpVerifiedTimestamp
  **/
  @ApiModelProperty(value = "")

  @Valid

  public DateTime getOtpVerifiedTimestamp() {
    return otpVerifiedTimestamp;
  }

  public void setOtpVerifiedTimestamp(DateTime otpVerifiedTimestamp) {
    this.otpVerifiedTimestamp = otpVerifiedTimestamp;
  }

  public Customer chatMessages(List<ChatMessage> chatMessages) {
    this.chatMessages = chatMessages;
    return this;
  }

  public Customer addChatMessagesItem(ChatMessage chatMessagesItem) {
    if (this.chatMessages == null) {
      this.chatMessages = new ArrayList<ChatMessage>();
    }
    this.chatMessages.add(chatMessagesItem);
    return this;
  }

  /**
   * Get chatMessages
   * @return chatMessages
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ChatMessage> getChatMessages() {
    return chatMessages;
  }

  public void setChatMessages(List<ChatMessage> chatMessages) {
    this.chatMessages = chatMessages;
  }

  public Customer chatMessageUnreadCount(BigDecimal chatMessageUnreadCount) {
    this.chatMessageUnreadCount = chatMessageUnreadCount;
    return this;
  }

  /**
   * Get chatMessageUnreadCount
   * @return chatMessageUnreadCount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getChatMessageUnreadCount() {
    return chatMessageUnreadCount;
  }

  public void setChatMessageUnreadCount(BigDecimal chatMessageUnreadCount) {
    this.chatMessageUnreadCount = chatMessageUnreadCount;
  }

  public Customer chatMessageLastReceivedTimestamp(DateTime chatMessageLastReceivedTimestamp) {
    this.chatMessageLastReceivedTimestamp = chatMessageLastReceivedTimestamp;
    return this;
  }

  /**
   * Get chatMessageLastReceivedTimestamp
   * @return chatMessageLastReceivedTimestamp
  **/
  @ApiModelProperty(value = "")

  @Valid

  public DateTime getChatMessageLastReceivedTimestamp() {
    return chatMessageLastReceivedTimestamp;
  }

  public void setChatMessageLastReceivedTimestamp(DateTime chatMessageLastReceivedTimestamp) {
    this.chatMessageLastReceivedTimestamp = chatMessageLastReceivedTimestamp;
  }

  public Customer chatMessageOldestUnreadTimestamp(DateTime chatMessageOldestUnreadTimestamp) {
    this.chatMessageOldestUnreadTimestamp = chatMessageOldestUnreadTimestamp;
    return this;
  }

  /**
   * Get chatMessageOldestUnreadTimestamp
   * @return chatMessageOldestUnreadTimestamp
  **/
  @ApiModelProperty(value = "")

  @Valid

  public DateTime getChatMessageOldestUnreadTimestamp() {
    return chatMessageOldestUnreadTimestamp;
  }

  public void setChatMessageOldestUnreadTimestamp(DateTime chatMessageOldestUnreadTimestamp) {
    this.chatMessageOldestUnreadTimestamp = chatMessageOldestUnreadTimestamp;
  }

  public Customer modelClass(String modelClass) {
    this.modelClass = modelClass;
    return this;
  }

  /**
   * Get modelClass
   * @return modelClass
  **/
  @ApiModelProperty(value = "")


  public String getModelClass() {
    return modelClass;
  }

  public void setModelClass(String modelClass) {
    this.modelClass = modelClass;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Customer customer = (Customer) o;
    return Objects.equals(this.id, customer.id) &&
        Objects.equals(this.chatappUsers, customer.chatappUsers) &&
        Objects.equals(this.createdTimestamp, customer.createdTimestamp) &&
        Objects.equals(this.updatedTimestamp, customer.updatedTimestamp) &&
        Objects.equals(this.employeeId, customer.employeeId) &&
        Objects.equals(this.isTagged, customer.isTagged) &&
        Objects.equals(this.isClosed, customer.isClosed) &&
        Objects.equals(this.otp, customer.otp) &&
        Objects.equals(this.otpTimestamp, customer.otpTimestamp) &&
        Objects.equals(this.otpSentTo, customer.otpSentTo) &&
        Objects.equals(this.otpVerifiedTimestamp, customer.otpVerifiedTimestamp) &&
        Objects.equals(this.chatMessages, customer.chatMessages) &&
        Objects.equals(this.chatMessageUnreadCount, customer.chatMessageUnreadCount) &&
        Objects.equals(this.chatMessageLastReceivedTimestamp, customer.chatMessageLastReceivedTimestamp) &&
        Objects.equals(this.chatMessageOldestUnreadTimestamp, customer.chatMessageOldestUnreadTimestamp) &&
        Objects.equals(this.modelClass, customer.modelClass);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, chatappUsers, createdTimestamp, updatedTimestamp, employeeId, isTagged, isClosed, otp, otpTimestamp, otpSentTo, otpVerifiedTimestamp, chatMessages, chatMessageUnreadCount, chatMessageLastReceivedTimestamp, chatMessageOldestUnreadTimestamp, modelClass);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Customer {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    chatappUsers: ").append(toIndentedString(chatappUsers)).append("\n");
    sb.append("    createdTimestamp: ").append(toIndentedString(createdTimestamp)).append("\n");
    sb.append("    updatedTimestamp: ").append(toIndentedString(updatedTimestamp)).append("\n");
    sb.append("    employeeId: ").append(toIndentedString(employeeId)).append("\n");
    sb.append("    isTagged: ").append(toIndentedString(isTagged)).append("\n");
    sb.append("    isClosed: ").append(toIndentedString(isClosed)).append("\n");
    sb.append("    otp: ").append(toIndentedString(otp)).append("\n");
    sb.append("    otpTimestamp: ").append(toIndentedString(otpTimestamp)).append("\n");
    sb.append("    otpSentTo: ").append(toIndentedString(otpSentTo)).append("\n");
    sb.append("    otpVerifiedTimestamp: ").append(toIndentedString(otpVerifiedTimestamp)).append("\n");
    sb.append("    chatMessages: ").append(toIndentedString(chatMessages)).append("\n");
    sb.append("    chatMessageUnreadCount: ").append(toIndentedString(chatMessageUnreadCount)).append("\n");
    sb.append("    chatMessageLastReceivedTimestamp: ").append(toIndentedString(chatMessageLastReceivedTimestamp)).append("\n");
    sb.append("    chatMessageOldestUnreadTimestamp: ").append(toIndentedString(chatMessageOldestUnreadTimestamp)).append("\n");
    sb.append("    modelClass: ").append(toIndentedString(modelClass)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

