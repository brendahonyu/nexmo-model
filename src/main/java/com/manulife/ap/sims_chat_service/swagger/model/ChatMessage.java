package com.manulife.ap.sims_chat_service.swagger.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.querydsl.core.annotations.QueryEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Defines a chat message information.
 */
@ApiModel(description = "Defines a chat message information.")
@Validated
@QueryEntity
@Document(collection = "chatMessages")
public class ChatMessage  implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @JsonProperty("id")
  private String id = null;

  @Indexed
  @JsonProperty("message_uuid")
  private String messageUuid = null;

  @JsonProperty("from")
  private User from = null;

  @JsonProperty("to")
  private User to = null;

  @JsonProperty("timestamp")
  private DateTime timestamp = null;

  @JsonProperty("direction")
  private String direction = null;

  @JsonProperty("message")
  private Message message = null;

  @JsonProperty("isRead")
  private Boolean isRead = null;

  @Indexed
  @JsonProperty("customerId")
  private String customerId = null;

  @JsonProperty("employeeId")
  private String employeeId = null;

  @JsonProperty("statuses")
  @Valid
  private List<ChatMessageStatus> statuses = null;

  @JsonProperty("modelClass")
  private String modelClass = null;

  public ChatMessage id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public ChatMessage messageUuid(String messageUuid) {
    this.messageUuid = messageUuid;
    return this;
  }

  /**
   * Get messageUuid
   * @return messageUuid
  **/
  @ApiModelProperty(value = "")


  public String getMessageUuid() {
    return messageUuid;
  }

  public void setMessageUuid(String messageUuid) {
    this.messageUuid = messageUuid;
  }

  public ChatMessage from(User from) {
    this.from = from;
    return this;
  }

  /**
   * Get from
   * @return from
  **/
  @ApiModelProperty(value = "")

  @Valid

  public User getFrom() {
    return from;
  }

  public void setFrom(User from) {
    this.from = from;
  }

  public ChatMessage to(User to) {
    this.to = to;
    return this;
  }

  /**
   * Get to
   * @return to
  **/
  @ApiModelProperty(value = "")

  @Valid

  public User getTo() {
    return to;
  }

  public void setTo(User to) {
    this.to = to;
  }

  public ChatMessage timestamp(DateTime timestamp) {
    this.timestamp = timestamp;
    return this;
  }

  /**
   * Get timestamp
   * @return timestamp
  **/
  @ApiModelProperty(value = "")

  @Valid

  public DateTime getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(DateTime timestamp) {
    this.timestamp = timestamp;
  }

  public ChatMessage direction(String direction) {
    this.direction = direction;
    return this;
  }

  /**
   * Get direction
   * @return direction
  **/
  @ApiModelProperty(value = "")


  public String getDirection() {
    return direction;
  }

  public void setDirection(String direction) {
    this.direction = direction;
  }

  public ChatMessage message(Message message) {
    this.message = message;
    return this;
  }

  /**
   * Get message
   * @return message
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Message getMessage() {
    return message;
  }

  public void setMessage(Message message) {
    this.message = message;
  }

  public ChatMessage isRead(Boolean isRead) {
    this.isRead = isRead;
    return this;
  }

  /**
   * Get isRead
   * @return isRead
  **/
  @ApiModelProperty(value = "")


  public Boolean isIsRead() {
    return isRead;
  }

  public void setIsRead(Boolean isRead) {
    this.isRead = isRead;
  }

  public ChatMessage customerId(String customerId) {
    this.customerId = customerId;
    return this;
  }

  /**
   * Get customerId
   * @return customerId
  **/
  @ApiModelProperty(value = "")


  public String getCustomerId() {
    return customerId;
  }

  public void setCustomerId(String customerId) {
    this.customerId = customerId;
  }

  public ChatMessage employeeId(String employeeId) {
    this.employeeId = employeeId;
    return this;
  }

  /**
   * Get employeeId
   * @return employeeId
  **/
  @ApiModelProperty(value = "")


  public String getEmployeeId() {
    return employeeId;
  }

  public void setEmployeeId(String employeeId) {
    this.employeeId = employeeId;
  }

  public ChatMessage statuses(List<ChatMessageStatus> statuses) {
    this.statuses = statuses;
    return this;
  }

  public ChatMessage addStatusesItem(ChatMessageStatus statusesItem) {
    if (this.statuses == null) {
      this.statuses = new ArrayList<ChatMessageStatus>();
    }
    this.statuses.add(statusesItem);
    return this;
  }

  /**
   * Get statuses
   * @return statuses
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ChatMessageStatus> getStatuses() {
    return statuses;
  }

  public void setStatuses(List<ChatMessageStatus> statuses) {
    this.statuses = statuses;
  }

  public ChatMessage modelClass(String modelClass) {
    this.modelClass = modelClass;
    return this;
  }

  /**
   * Get modelClass
   * @return modelClass
  **/
  @ApiModelProperty(value = "")


  public String getModelClass() {
    return modelClass;
  }

  public void setModelClass(String modelClass) {
    this.modelClass = modelClass;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ChatMessage chatMessage = (ChatMessage) o;
    return Objects.equals(this.id, chatMessage.id) &&
        Objects.equals(this.messageUuid, chatMessage.messageUuid) &&
        Objects.equals(this.from, chatMessage.from) &&
        Objects.equals(this.to, chatMessage.to) &&
        Objects.equals(this.timestamp, chatMessage.timestamp) &&
        Objects.equals(this.direction, chatMessage.direction) &&
        Objects.equals(this.message, chatMessage.message) &&
        Objects.equals(this.isRead, chatMessage.isRead) &&
        Objects.equals(this.customerId, chatMessage.customerId) &&
        Objects.equals(this.employeeId, chatMessage.employeeId) &&
        Objects.equals(this.statuses, chatMessage.statuses) &&
        Objects.equals(this.modelClass, chatMessage.modelClass);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, messageUuid, from, to, timestamp, direction, message, isRead, customerId, employeeId, statuses, modelClass);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ChatMessage {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    messageUuid: ").append(toIndentedString(messageUuid)).append("\n");
    sb.append("    from: ").append(toIndentedString(from)).append("\n");
    sb.append("    to: ").append(toIndentedString(to)).append("\n");
    sb.append("    timestamp: ").append(toIndentedString(timestamp)).append("\n");
    sb.append("    direction: ").append(toIndentedString(direction)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    isRead: ").append(toIndentedString(isRead)).append("\n");
    sb.append("    customerId: ").append(toIndentedString(customerId)).append("\n");
    sb.append("    employeeId: ").append(toIndentedString(employeeId)).append("\n");
    sb.append("    statuses: ").append(toIndentedString(statuses)).append("\n");
    sb.append("    modelClass: ").append(toIndentedString(modelClass)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

